/*jshint node: true*/

var express = require("express");
var morgan = require("morgan");
var path = __dirname + "/public/views/";

var app = express();

app.use(morgan("dev"));

app.use(express.static("public"));
app.use("/jquery", express.static("bower_components/jquery/dist"));

app.get("/", function (req, res) {
    res.sendFile(path + "index.html");
});

var server = app.listen(8000, function () {
    var host = server.address().address;
    var port = server.address.port;
    console.dir("Serwer działa na http://localhost:8000", host, port);
});