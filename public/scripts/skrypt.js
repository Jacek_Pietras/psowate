/*jshint strict: true, browser: true, jquery: true*/

$(document).ready(function () {
    "use strict";

    $("#clickMe").click(function () {
        $("img").fadeIn(1000);
        $("#picframe").slideToggle("slow");
    });
});